# Coding Challenge Reward System

 - Implement the solution using Ruby.

I put all logic of this task into services: `app/services/rewards`.
Web endpoint exists into: `app/controllers/rewards_controller.rb`
Follow my instructions for setup, tests, run, and feel free to contact me if you have any questions.

### Requirements:
 - `Ruby version >= 2.7.0`
 - `Bundler version 2.1.2`

### Install:
 - `bundle install`

### Tests:
 - `bundle exec rubocop`
 - `bundle exec rspec`

### Run:
 - `rails s`
 - `curl -X POST localhost:3000/rewards --data-binary @example.txt -H "Content-Type: text/plain"`

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Rewards::Calculator do
  describe '.calculate' do
    it 'calculates simple rewards' do
      str = <<-REWARD
        2022-02-01 09:41 A recommends B
        2022-02-02 09:41 B accepts
        2022-02-03 09:41 B recommends C
      REWARD
      rows = Rewards::Formatter.new(str).sorted_rows
      result = described_class.new(rows)
      expect(result.calculate).to eq('A' => 1)
    end

    it 'calculate complex rewards' do
      str = <<-REWARD
        2022-01-12 09:41 A recommends B
        2022-01-14 09:41 B accepts
        2022-01-16 09:41 B recommends C
        2022-01-17 09:41 C accepts
        2022-01-19 09:41 C recommends D
        2022-01-23 09:41 B recommends D
        2022-01-25 09:41 D accepts
      REWARD
      rows = Rewards::Formatter.new(str).sorted_rows
      result = described_class.new(rows)
      expect(result.calculate).to eq('A' => 1.75, 'B' => 1.5, 'C' => 1)
    end
  end
end
